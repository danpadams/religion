<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Node;
use App\Models\Parent_Topic;
use App\Models\Topic;
use App\Models\Author;
use App\Models\Book;
use App\Models\Topic_Node;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\infoChi\Sort;

class Topics extends Controller
{
    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Get List of Topics for chosing Parent node
     *
     * @return array
     */
    public function index()
    {
        $Topics = Parent_Topic::get()->toarray();
        $Topics[] = ['topic_id' => 0];
        foreach ($Topics as $key => $ret) {
            if (isset($ret['parent'])) {
                $display = Topic::getName($ret['parent']) . ': ';
            } else {
                $display = '';
            }
            $display .= Topic::getName($ret['topic_id']);
            $ret['name'] = $display;

            $retval[$ret['topic_id']] = $ret;
        }

        $name = array_column($retval, 'name');
        array_multisort($name, SORT_ASC, $retval);
        return $retval;
    }

    public function view($topic_id) {
        $retval = Topic::where('id', '=', $topic_id)
            ->with('parent')
            ->first()->toarray();

        $retval['parent_topic_id'] = $retval['parent']['id'];
        $retval['parent_id'] = $retval['parent']['parent'];
        return $retval;

    }
}
