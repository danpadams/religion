<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Author;
use App\Models\Node;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\infoChi\Sort;

class Authors extends Controller
{
    private $request;

    // Created as a class variable inside a method
    private $Nodes;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $retval = Author::orderBy('display', 'Asc')->get();
        return $retval;
    }

    public function data($author_id)
    {
        $Author = Author::where('id', '=', $author_id)
            ->first();
        $Author['Quotes'] = $this->getQuotes($author_id);

        return $Author;
    }

    private function getQuotes($author_id)
    {
        $Quotes = Node::where('type', '=', 2)->where('author_id', '=', $author_id)
            ->get();

        return $Quotes;
    }

}
