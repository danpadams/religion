<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Models\Topic;
use App\Models\Node;
use App\Models\Topic_Node;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\infoChi\Sort;

class Books extends Controller
{
    private $request;

    // Created as a class variable inside a method
    private $Nodes;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index($sort_order = 'Unsorted')
    {
        $sort_order = 'Protestant';
        switch ($sort_order) {
            case 'Protestant':
                break;
            default:
                $sort_order = 'Unsorted';
        }
        $retval = Book::where('abbrev', '=', '0')
            ->where($sort_order, '<>', '0')
            ->orderBy($sort_order, 'Asc')->get();
        return $retval;
    }

    public function data($book_id)
    {
        $Book = Book::where('ID', '=', $book_id)
            ->where('Abbrev', '=', '0')
            ->first()->toarray();
        $Book['ChapterData'] = $this->chapters($book_id, $Book['Chapters']);
        return $Book;
    }

    /**
     * @param $book_id
     * @return void
     */
    private function chapters($book_id, $numChapters)
    {
        $Verses = Node::where('Type', '=', '1')->get()->toarray();
        $retval = [];
        for ($i = 0; $i < $numChapters; $i++) {
            $retval [$i + 1] = 0;
        }


        for ($i = 0; $i < sizeof($Verses); $i++) {
            // Get Verse Information
            $Verses[$i] = json_decode($Verses[$i]['data'], true);

            // Eliminate Verses that do not matter
            if ($Verses[$i]['Book_Num'] != $book_id) continue;
            $retval[$Verses[$i]['Chapter_Start']] = 1;
        }

        return $retval;
    }

    public function verses($book_id, $chapter_id)
    {
        $Verses = Node::where('Type', '=', '1')
            ->with('topic_nodes')
//            ->with('topic_nodes.topic')
            ->get()->toarray();
        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($Verses, true));
        $retval = [];

        for ($i = 0; $i < sizeof($Verses); $i++) {
            // Get Verse Information
            if (!empty($Verses[$i]['topic_nodes'][0]['topic_id'])) {
                $topic_id = $Verses[$i]['topic_nodes'][0]['topic_id'];
            }
            $Verses[$i] = json_decode($Verses[$i]['data'], true);
            // Eliminate Verses that do not matter
            if ($Verses[$i]['Book_Num'] != $book_id) continue;
            if ($Verses[$i]['Chapter_Start'] != $chapter_id) continue;

            // Add Name of Book
            $Verses[$i]['Book_Name'] = Book::getName($Verses[$i]['Book_Num']);

            // Add Topic Info
            if (!empty($topic_id)) {
                $Verses[$i]['topic_id'] = $topic_id;
                $Verses[$i]['Topic_Name'] = Topic::getName($topic_id);
            }

            Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($Verses[$i], true));
            $retval[] = $Verses[$i];
        }

        return $retval;
    }
}
