<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Author;
use App\Models\Node;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\infoChi\Sort;

class Settings extends Controller
{
    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

//    public function index()
//    {
//        $retval = Author::orderBy('display', 'Asc')->get();
//        return $retval;
//    }

    public function getNumbers()
    {
        return array('numbers' => 1);
    }

    private function getQuotes($author_id)
    {
        $Quotes = Node::where('type', '=', 2)->where('author_id', '=', $author_id)
            ->get();

        return $Quotes;
    }

}
