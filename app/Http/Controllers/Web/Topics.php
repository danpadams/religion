<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Node;
use App\Models\Parent_Topic;
use App\Models\Topic;
use App\Models\Book;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\infoChi\Sort;

class Topics extends Controller
{
    private $request;

    // Created as a class variable inside a method
    private $Nodes;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index($topic_id = 0)
    {
        $retval = [];

        if ($topic_id) {
            $Topic = Topic::where('id', '=', $topic_id)->first();
            $retval['Topic'] = ['topic_id' => $topic_id, 'name' => $Topic->name];
        } else {
            $retval['Topic'] = ['topic_id' => $topic_id, 'name' => 'Base'];
        }
        $retval['Children'] = $this->getChildren($topic_id);
        $retval['Verses'] = $this->getNodes($topic_id, 1);
        $retval['Quotes'] = $this->getNodes($topic_id, 2);

        return $retval;
    }

    public function breadcrumbs($topic_id = 0)
    {
        $retval = [];

        // Safety Counter for loop
        $counter = 0;
        $orig_topic_id = $topic_id;
        $old_topic_id = 0;
        while ($topic_id != 0 && $old_topic_id != $topic_id && $counter < 5) {
            $counter++;
            $old_topic_id = $topic_id;

            $Topic_Parent = Parent_Topic::where('topic_id', '=', $topic_id)->first();
            $Topic = Topic::where('id', '=', $topic_id)->first();
            $data["id"] = $topic_id;
            $data['parent'] = $Topic_Parent->parent;
            $data['name'] = $Topic->name;

            if ($topic_id != $orig_topic_id) {
                $retval[] = $data;
            }
            $topic_id = $data['parent'];
        }
        if ($topic_id == 0) {
            Log::info(__METHOD__ . ':' . __LINE__ . '-');
            $retval[] = [
                'id' => $topic_id,
                'name' => "Base"
            ];
        }

        $retval = array_reverse($retval);
        return $retval;
    }

    private function getChildren($parent_id)
    {
        $retval = [];
        $topics = Topic::with('parents')->orderBy('Name')->get();
        foreach ($topics as $topic) {
            foreach ($topic['parents'] as $datum) {
                if ($datum['parent'] == $parent_id) {
                    $retval[] = $topic;
                }
            }
        }
        return $retval;
    }

    private function getNodesDB($topic_id)
    {
        $retval = [];
        $nodes = Node::with('authors')->with('topic_nodes')
            ->get();

        foreach ($nodes as $node) {
            foreach ($node['topic_nodes'] as $topic_node) {
                if ($topic_node['topic_id'] == $topic_id) {
                    $retval[$node['type']][] = $node;
                }
            }
        }

        $this->Nodes = $retval;
    }

    private function getNodes($topic_id, $type_id)
    {
        $retval = [];
        if (empty($this->Nodes)) {
            $this->getNodesDB($topic_id);
        }

        if (!empty($this->Nodes[$type_id])) {
            $retval = $this->Nodes[$type_id];
        }
        foreach ($retval as $key => $value) {
            // Grab & Format Author's Name
            if (!empty($value['authors']['display'])) {
                $retval[$key]['author_display'] = $value['authors']['display'];
            }
            // Grab Book Name
            if ($value['type'] == 1) {
                $data = json_decode($value['data'], true);
                $data['Book_Name'] = Book::getName($data['Book_Num']);
                $data['Book_Sort'] = Book::getSortOrder($data['Book_Num']);
                $retval[$key] = $data;
            }
        }
        //Sort the Data to be returned
        switch ($type_id) {
            case 1:
                $retval = Sort::multiSort($retval, 'Book_Sort', 'Chapter_Start');
                break;

        }

        return $retval;
    }

}
