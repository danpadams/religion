<?php

namespace App\Models;

use App\Models\infoChiModel;
use Illuminate\Support\Facades\Log;

class Book extends infoChiModel
{
    protected $table = 'books';
    private static $Books = [];
    private static $Book_Order = [];

    //put your code here
    protected $fillable = [
    ];

    private static function getData()
    {
        if (empty(self::$Books) || empty(self::$Book_Order)) {
            Log::info('Book Name Lookup');
            $List = self::select('id', 'Unsorted', 'Name')->where('abbrev', '=', '0')->get();
            foreach ($List as $Element) {
                self::$Books[$Element['id']] = $Element['Name'];
                self::$Book_Order[$Element['id']] = $Element['Unsorted'];
            }
        }

    }

    public static function getName($book_id): string
    {
        self::getData();
        if ($book_id) {
            $retval = self::$Books[$book_id];
        } else $retval = 'Book_' . $book_id;
        return $retval;
    }

    public static function getSortOrder($book_id): int
    {
        self::getData();
        if ($book_id) {
            $retval = self::$Book_Order[$book_id];
        } else $retval = $book_id;
        return $retval;

    }

}
