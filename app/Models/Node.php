<?php

namespace App\Models;

use App\Models\infoChiModel;
use App\Models\Topic_Node;
use App\Models\Author;
use Illuminate\Support\Facades\Log;

class Node extends infoChiModel
{
    protected $table = 'nodes';

    //put your code here
    protected $fillable = [
    ];

    public function topic_nodes()
    {
        return $this->hasMany(Topic_Node::class);
    }

    public function authors()
    {
        return $this->hasOne(Author::class, 'id', 'author_id');
    }
}
