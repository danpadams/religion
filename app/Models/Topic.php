<?php

namespace App\Models;

use App\Models\infoChiModel;
use App\Models\Parent_Topic;
use Illuminate\Support\Facades\Log;

class Topic extends infoChiModel
{
    protected $table = 'topics';
    private static $Topics = [];
    //put your code here
    protected $fillable = [
    ];

    private static function getData()
    {
        if (empty(self::$Topics)) {
            Log::info('Topic Name Lookup');
            $List = self::select('id', 'Name')->get();
            self::$Topics[0] = 'Base';
            foreach ($List as $Element) {
                self::$Topics[$Element['id']] = $Element['Name'];
            }
            Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r(self::$Topics, true));
        }

    }

    public static function getName($topic_id): string
    {
        self::getData();
        if (is_numeric($topic_id)) {
            $retval = self::$Topics[$topic_id];
        } else $retval = 'Topic' . $topic_id;
        return $retval;
    }

    public function parents()
    {
        return $this->hasMany(Parent_Topic::class);
    }

    public function parent()
    {
        return $this->belongsTo(Parent_Topic::class, 'id','topic_id');
    }
}
