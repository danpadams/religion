<?php

namespace App\infoChi;

// A Componen tpye class by Daniel Adams
// 2021-05-02

use phpDocumentor\Reflection\Types\Boolean;

class Sort
{
/*
 * Uses "usort" a Native PHP Function to do the sorting
 * sorting can be multiple elements like an SQL command:
 * ORDER BY soc, code
 *
    // Setup
    $array[] = array('soc' => 2, 'code'=>1);
    $array[] = array('soc' => 1, 'code'=>1);
    $array[] = array('soc' => 3, 'code'=>1);
    $array[] = array('soc' => 1, 'code'=>1);
    $array[] = array('soc' => 2, 'code'=>5);
    $array[] = array('soc' => 1, 'code'=>2);
    $array[] = array('soc' => 3, 'code'=>2);

     // Usage
     print_r(multiSort($array, 'soc', 'code'));
*/

    public static function multiSort()
    {
        //get args of the function
        $args = func_get_args();
        $c = count($args);
        if ($c < 2) {
            return false;
        }

        //get the array to sort
        $array = array_splice($args, 0, 1);
        $array = $array[0];

        //sort with an anoymous function using args
        usort($array, function($a, $b) use($args) {

            $i = 0;
            $c = count($args);
            $cmp = 0;
            while($cmp == 0 && $i < $c)
            {
                $cmp = self::cmp($a[$args[$i]], $b[$args[$i]]);
//                $cmp = strcmp($a[ $args[ $i ] ], $b[ $args[ $i ] ]);
                $i++;
            }

            return $cmp;

        });

        return $array;
    }

    private static function cmp($a, $b)
    {
        if ($a < $b) return -1;
        if ($a > $b) return 1;
        if ($a == $b) return 0;
    }
}