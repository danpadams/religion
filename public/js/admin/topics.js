// $location is for Future Development
infoChi.controller('adminTopicsCtrl', ['$scope', '$http', '$localStorage',
        '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams', '$sce',
        function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams, $sce) {
            getData = function () {
                console.log("getData()")
                if (topic_id != 0) {
                    infoChiSpinner.show();
                    $http.get('/admin/topics/' + topic_id).then(function (response) {
                        infoChiSpinner.hide();
                        $scope.Data = response.data;
                        if ($scope.Topics.length) {

                        }

                        console.log(response.data);
                    });
                } else {
                    $scope.Data = {id: 0, name: ''};
                }
                getTopics();
            }

            getTopics = function () {
                infoChiSpinner.show();
                $http.get('/admin/topics').then(function (response) {
                    $scope.Topics = response.data;
                    // console.log($scope.Topics);
                    infoChiSpinner.hide();
                });
            }

            $scope.updateParent = function (item) {
                console.log('updateParent()');
                console.log(item);
                $scope.Data.parent = item.topic_id;
            }

            $scope.saveData = function () {
                console.log('saveData() - WIP');
                if ($scope.Data.parent == undefined) {
                    return;
                }
                console.log($scope.Data);
            }

            $scope.startFresh = function () {
                console.log('startFresh() - WIP');
            }

            // --
            if ($routeParams.topic_id) {
                topic_id = $routeParams.topic_id;
            } else {
                topic_id = 0;
            }
            getData();
        }
    ]
);
