// $location is for Future Development
infoChi.controller('frontAuthorCtrl', ['$scope', '$http', '$localStorage',
        '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams', '$sce', 'infoChiSettings',
        function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams, $sce, infoChiSettings) {
            // infoChiFlash.show('This is a test.', 'success');
            getData = function () {
                infoChiSpinner.show(); // Chapter
                // infoChiSpinner.show(); // Verses
                $http.get('/web/authors/' + author_id).then(function (response) {
                    $scope.Data = response.data;
                    console.log($scope.Data);
                    infoChiSpinner.hide();
                });
            };

            $scope.displayNode = function (item) {
                var retval = '';
                if (infoChiSettings.getNumbers()) {
                    retval += item.id + " - ";
                }
                retval += '\"' + item.data + '\"';
                return retval;
            }


            // --
            var author_id = $routeParams.author_id;

            $scope.Data = {"Quote": []};
            getData();
        }]
);
