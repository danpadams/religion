// $location is for Future Development
infoChi.controller('frontAuthorsCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', 'infoChiDisplay', '$routeParams', '$sce',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, infoChiDisplay, $routeParams, $sce) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/authors').then(function (response) {
                Data = response.data;
                console.log(Data);
                infoChiSpinner.hide();
            });
        };

        $scope.bookSections = function (layout) {
            retval = infoChiDisplay.columnar(Data, 'Author');
            return $sce.trustAsHtml(retval);
        }

        // --
        var Data = [];
        getData();
    }]
);
