// $location is for Future Development
infoChi.controller('frontBookChapterCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', 'infoChiDisplay', '$routeParams', '$sce',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, infoChiDisplay, $routeParams, $sce) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            infoChiSpinner.show(); // Chapter
            // infoChiSpinner.show(); // Verses
            $http.get('/web/books/' + book_id).then(function (response) {
                $scope.Data = response.data;
                infoChiSpinner.hide();
            });
            infoChiSpinner.show(); // Verses
            $http.get('/web/books/' + book_id + '/' + $scope.chapter_id).then(function (response) {
                Verses = response.data;
                $scope.Verses = response.data;
                console.log('getData()');
                console.log($scope.Verses);
                infoChiSpinner.hide();
            });
        };

        $scope.bookChaptersOld = function (layout) {
            retval = '';
            if ($scope.Data.Chapters > 1) {
                for (i = 0; i < $scope.Data.Chapters; i++) {
                    if (i) {
                        retval += ' | ';
                    }
                    retval += '<a href="#!/book/' + book_id + '/' + (i + 1) + '">' + (i + 1) + '</a>';
                }
            }
            return $sce.trustAsHtml(retval);
        }
        $scope.bookChapters = function (layout) {
            retval = '';

            for (i = 0; i < $scope.Data.Chapters; i++) {
                if (i) {
                    retval += ' | ';
                }
                if ($scope.Data.ChapterData[i+1]) {
                    retval += '<a href="#!/book/' + book_id + '/' + (i + 1) + '">' + (i + 1) + '</a>';
                } else {
                    retval += (i + 1);
                }
            }


            return $sce.trustAsHtml(retval);
        }

        $scope.bookVerses = function (layout) {
            retval = infoChiDisplay.columnar($scope.Verses, 'BookVerse');
            return $sce.trustAsHtml(retval);
        }

        // --
        var book_id = $routeParams.book_id;
        if ($routeParams.chapter_id) {
            $scope.chapter_id = $routeParams.chapter_id;
        } else {
            $scope.chapter_id = 0;
        }

        $scope.Data = [];
        Verses = [];
        getData();
    }]
);
