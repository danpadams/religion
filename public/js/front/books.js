// $location is for Future Development
infoChi.controller('frontBooksCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', 'infoChiDisplay', '$routeParams', '$sce',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, infoChiDisplay, $routeParams, $sce) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/books').then(function (response) {
                Data = response.data;
                console.log($scope.data);
                infoChiSpinner.hide();
            });
        };

        $scope.bookSections = function (layout) {
            retval = infoChiDisplay.columnar(Data, 'Book');
            return $sce.trustAsHtml(retval);
        }

        // --
        var Data = [];
        getData();
    }]
);
