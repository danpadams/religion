// $location is for Future Development
infoChi.controller('frontTopicCtrl', ['$scope', '$http', '$localStorage',
        '$location', 'infoChiSpinner', 'infoChiFlash', 'infoChiDisplay', '$routeParams', '$sce',
        function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, infoChiDisplay, $routeParams, $sce) {
            // infoChiFlash.show('This is a test.', 'success');
            getData = function () {
                infoChiSpinner.show();
                $http.get('/web/topic/' + topic_id).then(function (response) {
                    $scope.data = response.data;
                    infoChiSpinner.hide();
                });
                getBreadcrumbs();
            };
            getBreadcrumbs = function () {
                if (topic_id != 0) {
                    console.log('getBreadcrumbs')
                    infoChiSpinner.show();
                    $http.get('/web/topic/' + topic_id + '/breadcrumbs').then(function (response) {
                        $scope.breadcrumbs = response.data;
                        console.log(response.data)
                        infoChiSpinner.hide();
                    });
                }
            }

            $scope.columnar = function (item, style, type) {
                if (item == undefined) {
                    return;
                }
                retval = infoChiDisplay.columnar(item, type);
                return $sce.trustAsHtml(retval);
            }

            empty = function (item) {
                if (item == undefined || item == 0) {
                    return true;
                }
                return false;
            }

            // --
            if ($routeParams.topic_id) {
                var topic_id = $routeParams.topic_id;
            } else {
                var topic_id = 0;
            }
            $scope.topic_id = topic_id;

            getData();
        }
    ]
);
