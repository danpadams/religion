/*
 This directive allows us to pass a function in on an enter key to do what we want.
 */
infoChi.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            // Enter
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
            // Space Bar
            if (event.which === 32) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
        });
    };
});

infoChi.factory('infoChiSpinner', ['spinnerService',
    function (spinnerService) {
        service = {};
        count = 0;

        service.show = function () {
            if (count == 0) {
                spinnerService.show('mainSpinner');
            }
            count++;
        };
        service.hide = function () {
            if (count > 0) {
                count -= 1;
                if (count == 0) {
                    spinnerService.hide('mainSpinner');
                }
            }
        };
        return service;
    }
]);

infoChi.factory('infoChiTooltip', ['$http', function ($http) {
    service = {};
    service.tooltip = function (tooltip_id) {
        return $http.get('/web/tooltip/view/' + tooltip_id).then(function (data) {

            retval = data.data.body;
            return retval;
        });
    };
    return service;
}
]);

infoChi.factory('infoChiTooltips', ['$localStorage',
    'infoChiTooltip',
    function ($localStorage, infoChiTooltipService) {
        service = {};
        service.tooltip = function (newId) {
//            window.localStorage.clear();
            $localStorage.$default({
                tooltips: {newId: null}
            });
            if ($localStorage.tooltips[newId] == undefined || $localStorage.tooltips[newId] == null) {
//                $localStorage.tooltips[newId] = '';
                infoChiTooltipService.tooltip(newId).then(function (response) {
                    console.log('#' + newId + ' : ' + response);
                    $localStorage.tooltips[newId] = response;

                });
            }
//            console.log($localStorage.tooltips[newId]);
            return $localStorage.tooltips[newId];

        };
        return service;
    }
]);

infoChi.factory('infoChiArray', [function () {
    service = {};
    service.in_array = function (needle, haystack, key_value, keys = []) {
        key0 = keys[0];

        for (var i = 0; i < haystack.length; i++) {
            if (haystack[i][key0][key_value] == needle) {
                return true;
            }
        }
        return false;
    };
    service.removeFromList = function (with_id, haystack) {
        var retval = [];
        for (var i = 0; i < haystack.length; i++) {
            if (haystack[i].id != with_id.id) {
                retval.push(haystack[i]);
            }
        }
        return retval;
    };
    service.setFromList = function (id, List) {
        for (var i = 0; i < List.length; i++) {
            if (List[i].id == id) {
                return List[i];
            }
        }
    };
    return service;
}
]);

// Function for filtering on a universal level
infoChi.factory('infoChiList', [function () {
    service = {};
    // Allow filter to be used in a generic arena
    filter = function (num, haystack, list_id) {
        list_id = typeof list_id !== 'undefined' ? list_id : '42';
        // Logic is to avoid items already in the list.
        if (haystack) {
            for (var i = 0; i < haystack.length; i++) {
                if (num == haystack[i][list_id])
                    return false;
            }
        }
        return true;
    };
    service.filter = function (num, haystack, list_id) {
        return filter(num, haystack, list_id);
    };
    service.exists = function (num, haystack, list_id) {
//            list_id = typeof list_id !== 'undefined' ? list_id : 'id';
        // Logic is to avoid items already in the list.
        if (haystack) {
            for (var i = 0; i < haystack.length; i++) {
                if (num == haystack[i][list_id]) {
                    return true;
                }
            }
        }
        return false;
    };
    // Allow filter to be tested to determine size of list
    service.filterAll = function (needles, key, haystack, list_id) {
        var num = 0;
        for (var i = 0; i < needles.length; i++) {
            if (filter(needles[i][key], haystack, list_id)) {
                num++;
            }
        }
        return num;
    };
    /**
     * type_id = the type of the Method to pick
     * detail = array rep for the data to pick within
     * choice = array of the dropdown boxes
     * runFilter = callback function to the filter item
     */
    service.setMethod = function (type_id, detail, choice, runFilter) {
        for (key = 0; key < detail.length; key++) {
            value = detail[key];
            if (runFilter(value)) {
                if (type_id) {
                    for (skey = 0; skey < value.PersonMethod.length; skey++) {
                        svalue = value.PersonMethod[skey];
                        if (svalue.person_method_type_id == type_id) {
                            choice[svalue.person_id] = svalue;
                        }
                    }
                } else {
                    choice[value.id] = $scope.no_method;
                    // Set NO Contact
                }
            }
        }

    };

    return service;
}
]);

infoChi.factory('infoChiSettings', ['$http', 'infoChiSpinner', function ($http, infoChiSpinner) {
    service = {};
    service.getNumbers = function () {
        return 1;
    }
    return service;
}]);

infoChi.factory('infoChiDisplay', [function () {
    service = {};
    service.columnar = function (item, type) {
        if (item == undefined) {
            return '';
        }
        switch (type) {
            case 'BookVerse':
                retval = display(item);
                break;
            default:
                retval = columnar(item, type);
                break;
        }
        return retval;
    };
    display = function (item) {
        retval = '';
        for (spot = 0; spot < item.length; spot++) {
            retval += '<div class="row">';
            retval += '<div class="col-sm-3">';
            retval += displayVerse(item[spot]);
            retval += '</div>';
            retval += '<div class="col-sm-3">';
            retval += displayTopic(item[spot]);
            retval += '</div>';
            retval += '</div>';
        }

        return retval;
    }
    getColumnCounts = function (total) {
        cols = 4;

        square = Math.trunc(total / cols);
        remainder = (total % cols);

        counts = [square, square, square, square];
        for (i = 0; i < remainder; i++) {
            counts[i]++
        }

        return counts;
    }
    getColumn = function (columnCounts, spot) {
        total = 0;
        for (i = 0; i < columnCounts.length; i++) {
            if (spot < (columnCounts[i] + total)) {
                return i;
            } else {
                total += columnCounts[i]
            }
        }
    }
    columnar = function (item, type) {
        cols = 4;
        rows = Math.ceil(item.length / cols);
        columnCounts = getColumnCounts(item.length);

        if (!rows) {
            return;
        }

        retval = '<div class="row">';
        column = -1;

        // console.log(itemsPerCol);
        for (spot = 0; spot < item.length; spot++) {
            if (getColumn(columnCounts, spot) != column) {
                // New Column
                column++;

                if (spot) {
                    retval += "</div>";
                }
                retval += '<div class="col-sm-3">';
            }
            // Debug for Columnar
            // retval += column + ' : ' + getColumn(columnCounts, spot) + ' : ';
            if (spot < item.length) {
                switch (type) {
                    case 'Verse':
                        retval += displayVerse(item[spot]);
                        break;
                    case 'Topic':
                        retval += '<a href="#!/topic/' + item[spot].id + '">' + item[spot].name + '</a>';
                        break;
                    case 'Book':
                        retval += '<a href="#!/book/' + item[spot].ID + '">' + item[spot].Name + '</a>';
                        break;
                    case 'Author':
                        retval += '<a href="#!/author/' + item[spot].id + '">' + item[spot].display + '</a>';
                        break;
                    default:
                        retval += item[spot];
                        break;
                }
                retval += '<br>';
            }
        }
        retval += '</div></div>';
        return retval;
    };
    displayTopic = function (item) {
        retval = '';
        if (item.topic_id) {
            retval = '<a href="#!/topic/' + item.topic_id + '">' + item.Topic_Name + '</a>'
        }
        return retval;
    }
    displayVerse = function (item) {
        Verse_End = false;
        // May need to be in a factory for the books section
        retval = '' + item.Book_Name + ' ' + item.Chapter_Start;
        if (item.Verse_Start) {
            retval += ':' + item.Verse_Start;
        }
        if (item.Chapter_End != 0 || item.Verse_End != 0) {
            retval += '-';
        }
        if ((item.Chapter_End) && (item.Chapter_End != '0')) {
            retval += item.Chapter_End + ':';
            Verse_End = true;
        }
        if (item.Verse_End != 0 || Verse_End) {
            retval += item.Verse_End;
        }

        return retval;
    }
    return service;
}]);

