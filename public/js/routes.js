infoChi.config(function ($routeProvider) {
    $routeProvider.when('/topic/:topic_id', {
        templateUrl: "/html/front/topic.html",
        controller: "frontTopicCtrl"
    });
    $routeProvider.when('/books', {
        templateUrl: "/html/front/books.html",
        controller: "frontBooksCtrl"
    });
    $routeProvider.when('/book/:book_id', {
        templateUrl: "/html/front/book.html",
        controller: "frontBookChapterCtrl"
    });
    $routeProvider.when('/book/:book_id/:chapter_id', {
        templateUrl: "/html/front/book.html",
        controller: "frontBookChapterCtrl"
    });

    $routeProvider.when('/authors', {
        templateUrl: "/html/front/authors.html",
        controller: "frontAuthorsCtrl"
    });
    $routeProvider.when('/author/:author_id', {
        templateUrl: "/html/front/author.html",
        controller: "frontAuthorCtrl"
    });

    $routeProvider.when('/admin/nodes/:node_id', {
        templateUrl: "/html/admin/node.html",
        controller: "adminNodesCtrl"
    });
    $routeProvider.when('/admin/topics/:topic_id', {
        templateUrl: "/html/admin/topic.html",
        controller: "adminTopicsCtrl"
    });
    $routeProvider.when('/admin/authors/:author_id', {
        templateUrl: "/html/admin/author.html",
        controller: "adminAuthorsCtrl"
    });


    $routeProvider.otherwise({
        templateUrl: "/html/front/topic.html",
        controller: "frontTopicCtrl"
    })
});
