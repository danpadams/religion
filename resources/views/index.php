<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>infoChi Concordance</title>
    <script type="text/javascript" src="js/jquery-1.11.3.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-route.js"></script>
    <script type="text/javascript" src="lib/ui-bootstrap-tpls-2.5.0.min.js"></script>
    <!--    <script type="text/javascript" src="js/jquery.autocomplete.js"></script>-->
    <!--    <script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>-->
    <!--	<script type="text/javascript" src="js/jquery.tablesorter.pager.js"></script>-->
    <!--	<script type="text/javascript" src="js/jquery.metadata.js"></script>-->
    <!--	<script type="text/javascript" src="js/jquery.validate.min.js"></script>-->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular-sanitize.js"></script>
    <!--	<script type="text/javascript" src="js/notifyBox.js"></script>-->
    <!--	<script type="text/javascript" src="js/queueRunner.js"></script>-->
    <!--	<script type="text/javascript" src="js/jquery-ui-1.7.1/ui.core.min.js"></script>-->
    <!--	<script type="text/javascript" src="js/jquery-ui-1.7.1/ui.accordion.min.js"></script>-->

    <script type="text/javascript" src="/lib/ngStorage.js"></script>
    <script type="text/javascript" src="/lib/select.min.js"></script>
    <script type="text/javascript" src="/lib/angular-spinners.min.js"></script>
    <script type="text/javascript" src="/lib/angular-flash.js"></script>

    <!--    <script type="text/javascript" src="/lib/dirPagination.js"></script>-->


    <!--    <script type="text/javascript" src="/lib/angular-route.js"></script>-->
    <!--    <script type="text/javascript" src="/lib/ngStorage.js"></script>-->
    <!-- Style Sheets-->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/selectize.default.css">
    <link rel="stylesheet" type="text/css" href="css/selectize.css">
    <link rel="stylesheet" type="text/css" href="css/infochi.css">

    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script type="text/javascript">
        var infoChi = angular.module("infoChi", [
            "ngRoute",
            "ngStorage",
            "ngFlash",
            'ngSanitize',
            'ui.select',
            'angularSpinners',
            // 'angularUtils.directives.dirPagination',
            // 'ui.bootstrap.datetimepicker'
            'ui.bootstrap'
        ]);
    </script>

    <script type="text/javascript" src="js/routes.js"></script>
    <script type="text/javascript" src="js/infoChi.js"></script>
    <script type="text/javascript" src="js/infoChi-flash.js"></script>

    <script type="text/javascript" src="js/front/topics.js"></script>
    <script type="text/javascript" src="js/front/books.js"></script>
    <script type="text/javascript" src="js/front/book.js"></script>
    <script type="text/javascript" src="js/front/authors.js"></script>
    <script type="text/javascript" src="js/front/author.js"></script>

    <script type="text/javascript" src="js/admin/nodes.js"></script>
    <script type="text/javascript" src="js/admin/topics.js"></script>
    <script type="text/javascript" src="js/admin/authors.js"></script>
</head>
<body ng-app="infoChi" style="margin: 0">
<spinner name="mainSpinner" class="spinner">
    <h1>
        <i class="fa fa-spinner fa-spin fa-5x"></i><br>
        <span>Please Wait!</span>
    </h1>
</spinner>
<div class="container-fluid">
    <img alt="infoChi Christian Computing" src="img/jesusinside.gif" border="0" align="right">
    <h1 align="center">infoChi Concordance</h1>
    <div class="row">
        <div class="col-sm-3">
            <h3>View</h3>
            <p>
                <a href="#!/topic/0">Topic</a><br>
                <a href="#!/books">Book</a><br>
                <a href="#!/authors">Authors</a><br>
            </p>
            <h3>Edit/Create</h3>
            <p>
                <a href="#!/admin/nodes/0">Nodes</a><br>
                <a href="#!/admin/topics/0">Topics</a><br>
                <a href="#!/admin/authors/0">Authors</a><br>
            </p>
        </div>

        <div class="col-sm-9">
            <flash-message>
                <div class="well"></div>
            </flash-message>
            <ng-view></ng-view>
        </div>
    </div>
</div>
</body>
</html>
