<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // Show /home
//    return view('home');
    return View::make('index');
});

Route::group(['namespace' => 'Web'], function () {
    Route::get('/web/books', 'Books@index');
    Route::get('/web/books/{book_id}', 'Books@data');
    Route::get('/web/books/{book_id}/{chapter_id}', 'Books@verses');

    Route::get('/web/authors', 'Authors@index');
    Route::get('/web/authors/{author_id}', 'Authors@data');

    Route::get('/web/topic', 'Topics@index');
    Route::get('/web/topic/{topic_id}', 'Topics@index');
    Route::get('/web/topic/{topic_id}/breadcrumbs', 'Topics@breadcrumbs');

    Route::get('/web/settings/numbers', 'Settings@getNumbers');
});

Route::group(['namespace' => 'Admin'], function () {
    Route::post('/admin/node/saveData', 'Nodes@save_data');
    Route::get('/admin/node/types', 'Nodes@types');
    Route::get('/admin/node/books', 'Nodes@books');
    Route::get('/admin/node/authors', 'Nodes@authors');
    Route::get('/admin/node/{node_id}', 'Nodes@index');

    Route::get('/admin/topics', 'Topics@index');
    Route::get('/admin/topics/{topic_id}', 'Topics@view');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
